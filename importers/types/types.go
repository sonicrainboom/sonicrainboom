package types

import (
	"errors"
	"github.com/sirupsen/logrus"
)

type ImportFunc func(url string, logger logrus.FieldLogger) error

var ENoImporter = errors.New("no matching importer found")
