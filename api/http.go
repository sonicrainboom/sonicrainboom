package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/T4cC0re/silo/v2/sdk"
	silo_server "gitlab.com/T4cC0re/silo/v2/server/silo"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/auth"
	"gitlab.com/sonicrainboom/sonicrainboom/buildinfo"
	"gitlab.com/sonicrainboom/sonicrainboom/config"
	"gitlab.com/sonicrainboom/sonicrainboom/generated_assets/frontend_dist_sonicrainboom"
	"gitlab.com/sonicrainboom/sonicrainboom/library_management"
	srb_log "gitlab.com/sonicrainboom/sonicrainboom/log"
	"gitlab.com/sonicrainboom/sonicrainboom/messagehub"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata/types"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends"
	storage_types "gitlab.com/sonicrainboom/sonicrainboom/storage_backends/types"
	types2 "gitlab.com/sonicrainboom/sonicrainboom/storage_backends/types"
	"gitlab.com/sonicrainboom/sonicrainboom/transcoders"
	transcoder_types "gitlab.com/sonicrainboom/sonicrainboom/transcoders/types"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var aut auth.Auth

type MuxGlue struct {
	*mux.Router
	featureSets []string
}

func (r *MuxGlue) AddEndpoint(name string, endpoint http.HandlerFunc, acl string, methods ...string) {
	log.Info("pluginGlue code called")
	r.HandleFunc(name, endpoint).Methods(methods...).Name(acl)
}

/**
RouteHandler is used in static_file and static_zip to make sure, that 404s will load index.html.
	This happens when a user accesses a Angular controlled route.
*/
type RouteHandler struct {
	FS http.FileSystem
}

func (i RouteHandler) Open(name string) (file http.File, err error) {
	// Strip a leading `/ui`
	name = regexp.MustCompile(`(^/ui)`).ReplaceAllString(name, "")
	log.Warnf("Requested file %s", name)
	if file, err = i.FS.Open(name); err == nil {
		return
	}
	if os.IsNotExist(err) && !strings.HasPrefix(name, "/api/") {
		log.Warnf("Path defaulting to index.html: %s", name)
		return i.FS.Open("/index.html")
	}
	return
}

func Run() (chan error, chan struct{}, *http.Server) {
	var initErrors = make(chan error)
	var allSystemsGo = make(chan struct{})
	var server *http.Server = nil
	conf := config.GetConfig()

	go func() {
		r := MuxGlue{Router: mux.NewRouter(), featureSets: []string{"v1"}}
		aut = auth.New()
		messagehub.SetAuth(&aut)

		var silo *silo_server.Silo
		var siloLogger = srb_log.Subsystem("silo")

		if conf.BuiltInSilo {
			var siloBaseURL = "/api/v1/silo"

			silo, err := silo_server.NewSilo(conf.SiloPath, conf.SiloSecret, siloBaseURL, siloLogger)
			go func() {
				_ = silo.Persist()
			}()
			transcoders.SetSilo(silo.EmbeddedSDK(siloBaseURL))
			initErrors <- err
			r.PathPrefix(siloBaseURL).Handler(silo).Name("siloURL")
			aut.AddACL("siloURL", "public")
		} else {
			log.Warn("External Silo installation might cause same-origin policy errors")
			transcoders.SetSilo(sdk.NewSiloSDKHTTP(conf.SiloURL, conf.SiloSecret, siloLogger))
		}

		transcoders.Ready()

		r.Use(func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				if strings.HasPrefix(r.URL.Path, "/api") {
					w.Header().Set("content-type", "application/json; charset=utf-8")
					w.Header().Set("Access-Control-Allow-Credentials", "true")
					w.Header().Set("Access-Control-Allow-Origin", "*")
				}
				if strings.HasSuffix(r.URL.Path, ".js") {
					w.Header().Set("content-type", "application/javascript; charset=utf-8")
				}
				if strings.HasSuffix(r.URL.Path, "/webworker.js") {
					w.Header().Set("Service-Worker-Allowed", "/")
				}

				next.ServeHTTP(w, r)
			})
		})

		r.HandleFunc("/api/v1/messagehub", messagehub.Instance.Endpoint).Name("messageHub")

		r.HandleFunc("/api/v1/system", systemReport).Methods("GET", "HEAD").Name("administrative")

		// Auth
		r.HandleFunc("/api/v1/auth", aut.LoggedIn).Methods("GET").Name("auth")
		r.HandleFunc("/api/v1/auth/playerauth", aut.GetPlayerAuth).Methods("GET").Name("auth")
		r.HandleFunc("/api/v1/auth/logout", aut.Logout).Methods("GET").Name("auth")
		r.HandleFunc("/api/v1/auth/providers", aut.ListProviders).Methods("GET").Name("auth")
		r.HandleFunc("/api/v1/auth/providers/{provider:[a-z0-9]+}", aut.LoginByOauth).Methods("GET").Name("auth")
		r.HandleFunc("/api/v1/auth/callback", aut.Callback).Methods("GET").Name("auth")

		// Library endpoints
		r.HandleFunc("/api/v1/scan", scan).Methods("POST").Name("initScan")
		r.HandleFunc("/api/v1/scan", queueList).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/library", addLibrary).Methods("POST").Name("manageLibrary")
		r.HandleFunc("/api/v1/imports", addImport).Methods("POST").Name("manageLibrary")
		r.HandleFunc("/api/v1/imports", listImports).Methods("GET").Name("manageLibrary")

		// Artist endpoints
		r.HandleFunc("/api/v1/artists", artistList).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/artists/{id:[0-9]+}", artistInfo).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/artists/{id:[0-9]+}/albums", artistAlbums).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/artists/{id:[0-9]+}/merge/{mergeIds:[0-9,]+}", artistMerge).Methods("POST").Name("manageLibrary")

		// Album endpoints
		r.HandleFunc("/api/v1/albums", albumList).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/albums/{id:[0-9]+}", albumInfo).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/albums/{id:[0-9]+}/songs", albumSongs).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/albums/{id:[0-9]+}/cover", albumCover).Methods("GET", "HEAD").Name("showLibrary")

		// Song endpoints
		r.HandleFunc("/api/v1/songs", songList).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/songs/{id:[0-9]+}", songInfo).Methods("GET", "HEAD").Name("showLibrary")
		r.HandleFunc("/api/v1/songs/{id:[0-9]+}/played", streamAck).Methods("POST").Name("ackStream")
		r.HandleFunc("/api/v1/songs/{id:[0-9]+}/play/{targetFormat}", streamSong).Methods("GET", "HEAD").Name("streamSong")
		r.HandleFunc("/api/v1/songs/{id:[0-9]+}/convert/{targetFormat}", convertSong).Methods("GET", "HEAD").Name("convertSong")
		r.HandleFunc("/api/v1/versions/{id:[0-9]+}", versionInfo).Methods("GET", "HEAD").Name("streamSong")
		r.HandleFunc("/api/v1/versions/{id:[0-9]+}/raw", streamVersion).Methods("GET", "HEAD").Name("streamSong")

		// Share endpoints
		r.HandleFunc(" /shared/{key:[a-z0-9-]+}", apiInfo).Methods("GET", "HEAD").Name("public")

		// Misc
		r.HandleFunc("/api/v1/mimetypes", mimeTypes).Methods("GET", "HEAD").Name("public")
		r.HandleFunc("/api", apiInfo).Methods("GET", "HEAD").Name("apiVersion")

		// Plugins
		log.Debug("Initializing plugin API endpoints...")
		//err, featureSets := plugin_system.RegisterAPIs(r.AddEndpoint)
		//r.featureSets = append(r.featureSets, featureSets...)
		//initErrors <- err
		log.Debug("Initializing plugin API endpoints done ...")

		// Prometheus
		r.PathPrefix("/metrics").Handler(promhttp.Handler()).Name("metrics")

		// This has to be last!
		r.PathPrefix("/").
			Handler(http.FileServer(RouteHandler{frontend_dist_sonicrainboom.AssetFile()})).
			Name("static")

		r.NotFoundHandler = http.HandlerFunc(handleNotFound)

		// TODO: Do not use names for route ACL types, but rather assign ACLs directly to routes.
		// Public ACLs
		aut.AddACL("metrics", "public", "centralized_metrics")
		aut.AddACL("static", "public")
		aut.AddACL("apiVersion", "public")
		aut.AddACL("auth", "public")

		// Stream ACLs
		aut.AddACL("streamSong", "user")
		aut.AddACL("ackStream", "user")
		aut.AddACL("convertSong", "converter")

		// Library ACLs
		aut.AddACL("showLibrary", "user")
		aut.AddACL("initScan", "admin")
		aut.AddACL("manageLibrary", "admin")

		// Admin ACLs
		aut.AddACL("administrative", "admin")

		// MessageHub ACLs
		aut.AddACL("messageHub", "user")
		aut.AddACL("playerControl", "player") // The 'player' role is never assigned!

		r.Use(aut.Middleware)

		server = serve(r, silo, initErrors, allSystemsGo)
	}()

	return initErrors, allSystemsGo, server
}

func handleNotFound(writer http.ResponseWriter, request *http.Request) {
	route := mux.CurrentRoute(request)
	if route == nil {
		// No registered route. So not `/api` nor `/ui`
		log.Warnf("URL: %s", request.URL.String())
	} else {
		reg, _ := route.GetPathRegexp()
		log.Warnf("URL w/ route: %s %s", request.URL.String(), reg)
	}

	log.Warnf("Worker? %t", request.Header.Get("X-Sonicrainboom-Preflight") == "worker")
	log.Warnf("Preload? %t", request.Header.Get("Service-Worker-Navigation-Preload") == "true")
	log.Warnf("Headers: %+v", request.Header)

	switch true {
	case strings.EqualFold(request.URL.Path, "/ui"), strings.EqualFold(request.URL.Path, "/"):
		writer.Header().Set("Location", "/ui/")
		writer.WriteHeader(http.StatusFound)
	default:
		writer.WriteHeader(http.StatusNotFound)
	}
}

func albumCover(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")
	writer.Header().Set("Cache-Control", "public,max-age=3600,proxy-revalidate")
	//writer.Header().Set("ETag", fileHash) // TODO: Use cover file's hash as ETag
	writer.Header().Set("Location", "https://via.placeholder.com/1280?id="+vars["id"])
	writer.WriteHeader(http.StatusFound)
}

func systemReport(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("x-sonicrainboom-workercache", "ignore")
	writer.Header().Set("content-type", "text/plain; charset=utf-8")
	//writer.Header().Set("Content-Disposition", "attachment; filename=\"srb_report.md\"")

	_, _ = writer.Write(
		[]byte(`### SonicRainBoom System Report

##### Software Information
Software Version: ` + buildinfo.Version + `
Go runtime: ` + runtime.Version() + `

##### System Information
CPUs: ` + strconv.Itoa(runtime.NumCPU()) + `
OS: ` + buildinfo.GetRuntimeOSInfo() + `
`),
	)
}

func queueList(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	lastId, _ := strconv.Atoi(r.URL.Query().Get("lastId"))
	flags, _ := strconv.Atoi(r.URL.Query().Get("flags"))
	probe_queueEntries, lastId, err := library_management.Library.FetchProbeQueueByToggledFlags(flags, lastId)

	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	foo := struct {
		Data   []*library_management.ProbeQueueEntry
		LastId int
	}{
		Data:   probe_queueEntries,
		LastId: lastId,
	}

	j, err := json.Marshal(foo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func mimeTypes(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())

	j, err := json.Marshal(transcoders.GetMimes())
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func songList(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	lastId, _ := strconv.Atoi(r.URL.Query().Get("lastId"))
	songs, lastId, err := library_management.Library.FetchSongPaginated(lastId)

	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	foo := struct {
		Data   []*library_management.Song
		LastId int
	}{
		Data:   songs,
		LastId: lastId,
	}

	j, err := json.Marshal(foo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func artistList(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	lastId, _ := strconv.Atoi(r.URL.Query().Get("lastId"))
	artists, lastId, err := library_management.Library.FetchArtistPaginated(lastId)

	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	foo := struct {
		Data   []*library_management.Artist
		LastId int
	}{
		Data:   artists,
		LastId: lastId,
	}

	j, err := json.Marshal(foo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func albumList(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	lastId, _ := strconv.Atoi(r.URL.Query().Get("lastId"))
	albums, lastId, err := library_management.Library.FetchAlbumPaginated(lastId)

	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var albums2 = make([]*library_management.AlbumPopulated, len(albums))

	for i, album := range albums {
		var populated *library_management.AlbumPopulated
		populated, err = album.Populate()
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		albums2[i] = populated
	}

	foo := struct {
		Data   []*library_management.AlbumPopulated
		LastId int
	}{
		Data:   albums2,
		LastId: lastId,
	}

	j, err := json.Marshal(foo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func songInfo(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	song, err := library_management.Library.FetchSongById(id)
	if err != nil || song.Id == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	populated, err := song.Populate()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	j, err := json.Marshal(populated)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func versionInfo(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	version, err := library_management.Library.FindSongVersionById(id)
	if err != nil || version.Id == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	j, err := json.Marshal(version)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func albumSongs(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	songs, err := library_management.Library.FetchSongsByAlbum(id)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	songsP := make([]*library_management.SongPopulated, len(songs))
	for idx, song := range songs {
		populated, err := song.Populate()
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		songsP[idx] = populated
	}

	j, err := json.Marshal(songsP)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func streamAck(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	w.Header().Set("x-sonicrainboom-workercache", "ignore")
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = library_management.Library.IncreasePlayCount(id)
	//TODO Backend interaction? I.e. Last.fm scrobbles, etc.?
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func artistAlbums(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	albums, err := library_management.Library.FetchAlbumsByArtist(id)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	var albums2 = make([]*library_management.AlbumPopulated, len(albums))

	for i, album := range albums {
		var populated *library_management.AlbumPopulated
		populated, err = album.Populate()
		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		albums2[i] = populated
	}

	j, err := json.Marshal(albums2)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, _ = w.Write(j)
}

func artistMerge(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	mergeIDsString := vars["mergeIds"]
	mergeIDsStringSlice := strings.Split(mergeIDsString, ",")
	var mergeIDs []int
	for _, mergeIDString := range mergeIDsStringSlice {
		mergeID, err := strconv.Atoi(mergeIDString)
		if err != nil {
			log.Errorln(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		mergeIDs = append(mergeIDs, mergeID)
	}

	log.Infof("Merging Artists %+v into %d", mergeIDs, id)

	artist, err := library_management.Library.FetchArtistById(id)
	if err != nil || artist.Id == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	err = artist.MergeWithOtherArtists(mergeIDs...)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func albumInfo(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	album, err := library_management.Library.FetchAlbumById(id)
	if err != nil || album.Id == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	populated, err := album.Populate()
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	j, err := json.Marshal(populated)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func artistInfo(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	vars := mux.Vars(r)
	log.Infof("%+v", vars)
	log.Infof("%+v", r.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	artist, err := library_management.Library.FetchArtistById(id)
	if err != nil || artist.Id == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	j, err := json.Marshal(artist)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func apiInfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("x-sonicrainboom-workercache", "ignore")
	defer timetrack.TimeTrack(time.Now())

	w.Write([]byte(`{
  "product": "SonicRainBoom",
  "apiVersion": 1,
  "supported_apis": [
    "v1"
  ]
}`))
}

func scan(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("x-sonicrainboom-workercache", "ignore")
	defer timetrack.TimeTrack(time.Now())
	go func() {
		_, err := library_management.Library.Scan()
		if err != nil {
			log.Error(err)
		}
	}()
	w.WriteHeader(http.StatusCreated)
}

func listImports(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	w.Header().Set("x-sonicrainboom-workercache", "ignore")
	lastId, _ := strconv.Atoi(r.URL.Query().Get("lastId"))
	flags, _ := strconv.Atoi(r.URL.Query().Get("flags"))
	entries, lastId, err := library_management.Library.FetchImportQueueByToggledFlags(flags, lastId)
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	foo := struct {
		Data   []*library_management.ImportQueueEntry
		LastId int
	}{
		Data:   entries,
		LastId: lastId,
	}

	j, err := json.Marshal(foo)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(j)
}

func addImport(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	defer r.Body.Close()
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var data ImportURL
	err = json.Unmarshal(b, &data)
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if data.URL == "" {
		log.Error("tried to add empty URL to importer")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var id int64
	id, err = library_management.Library.AddImport(data.URL)
	if err != nil {
		log.Error(err)
		if strings.HasPrefix(err.Error(), "Error 1062") {
			w.WriteHeader(http.StatusConflict)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}
	type adhoc struct {
		Id int64 `json:id`
	}
	b, err = json.Marshal(adhoc{id})
	if err != nil {
		log.Error("marshalling json failed")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, _ = w.Write(b)
}

func addLibrary(w http.ResponseWriter, r *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	defer r.Body.Close()
	w.Header().Set("x-sonicrainboom-workercache", "ignore")
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
	}
	var data AddLibrary
	err = json.Unmarshal(b, &data)
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
	}
	err = library_management.Library.EnsureLibrary(data.Path)
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusOK)
}

func convertSong(response http.ResponseWriter, request *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	response.Header().Set("x-sonicrainboom-workercache", "ignore")
	vars := mux.Vars(request)
	log.Infof("%+v", vars)
	log.Infof("%+v", request.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	versions, err := library_management.Library.FindSongVersionsForSong(id)
	if err != nil || len(versions) < 1 {
		log.Errorln(err)
		response.WriteHeader(http.StatusNotFound)
		return
	}
	library_management.Library.RankVersionsByQuality(versions)

	var coded string
	var meta *types.Stream
	var ok bool
	for _, version := range versions {
		uri, perm, err := storage_backends.Retrieve(version.Accessor)
		if err != nil {
			log.Warn(err)
			continue
		}
		err = perm.EnsureConvert()
		if err != nil {
			log.Warn(err)
			continue
		}
		log.Infof("v: %+v, u: %s, e: %+v", version, uri, err)
		//if strings.HasPrefix(uri, "file://") {
		//	uri = uri[7:]
		//}

		coded, err = transcoders.ScheduledTranscode(vars["targetFormat"], uri)
		if err != nil {
			continue
		}

		metaFull, err := metadata.ExtractMedatata(coded)
		if err != nil {
			continue
		}

		for _, stream := range metaFull.Streams {
			if stream.CodecType == "audio" {
				meta = &stream
			}
		}
		ok = true
	}

	if !ok {
		log.Errorln(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	be, err := storage_backends.GetBackendByName("local")
	accessor, err := be.Save(coded)
	ver, err := library_management.Library.AddSongVersion(id, vars["targetFormat"], accessor, meta)

	j, err := json.Marshal(ver)
	response.Write(j)
}

func streamVersion(response http.ResponseWriter, request *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	response.Header().Set("x-sonicrainboom-workercache", "ignore")

	vars := mux.Vars(request)
	log.Infof("%+v", vars)
	log.Infof("%+v", request.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	// Try to get the proper version first
	version, err := library_management.Library.FindSongVersionById(id)
	if err != nil && err != library_management.ENoVersion {
		log.Errorln(err)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	err = serveVersionRaw(version, "source", response, request)
	if err != nil {
		log.Errorln(err)
		response.WriteHeader(http.StatusInternalServerError)
	}
}

func streamSong(response http.ResponseWriter, request *http.Request) {
	defer timetrack.TimeTrack(time.Now())
	response.Header().Set("x-sonicrainboom-workercache", "ignore")

	vars := mux.Vars(request)
	log.Infof("%+v", vars)
	log.Infof("%+v", request.Header)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Errorln(err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	format := ""
	ok := false
	if format, ok = vars["targetFormat"]; !ok || format == "raw" {
		// This will cause a raw stream of the best source
		format = ""
	}

	// Try to get the proper version first
	versions, err := library_management.Library.FindSingVersionsForSongAndFormat(id, format)
	if err != nil && err != library_management.ENoVersion {
		log.Errorln(err)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	log.Infoln(versions)

	library_management.Library.RankVersionsByQuality(versions)

	for _, version := range versions {
		err := serveVersionRaw(version, format, response, request)
		if err != nil {
			// Log the error and try again.
			log.Error(err)
		} else {
			// No error? We're done. It was streamed
			return
		}
	}

	versions, err = library_management.Library.FindSongVersionsForSong(id)
	if err != nil {
		log.Errorln(err)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	library_management.Library.RankVersionsByQuality(versions)

	var uri string
	var perms storage_types.PermissionSet
	for _, version := range versions {
		log.Info(version)
		uri, perms, err = storage_backends.Retrieve(version.Accessor)
		if err != nil {
			log.Warnf("Song version inaccessible: '%d'", version.Id)
			continue
		}
		err = perms.EnsureConvert()
		if err != nil {
			log.Warn(err)
			continue
		}

		response.Header().Set("Content-Type", transcoders.GetMime(format))
		err = transcoders.OndemandTranscode(format, uri, response, request)
		if err != nil {
			log.Errorf("OndemandTranscode failed %s", err)
			continue
		}
		return
	}

	log.Error(err)

	if err == transcoder_types.ENoTranscoder {
		// A client should query supported transcode formats first.
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	log.Error(err)

	response.WriteHeader(http.StatusInternalServerError)
	return
}

func serveVersionRaw(version *library_management.SongVersion, format string, response http.ResponseWriter, request *http.Request) (err error) {
	var link string
	var perms types2.PermissionSet
	link, perms, err = storage_backends.Retrieve(version.Accessor)
	if err != nil {
		return
	} else {
		err = perms.EnsureStream()
		if err != nil {
			return
		}
		// We can do this, as .Retrieve guarantees http(s) or file URLs
		if !strings.HasPrefix(link, "file://") {
			response.Header().Set("Location", link)
			response.Header().Set("x-sonicrainboom-transcode", "redirect")
			response.WriteHeader(http.StatusTemporaryRedirect)
			return
		}
		file := link[7:]

		var handle *os.File
		handle, err = os.Open(file)
		if err != nil {
			return
		}

		if transcoders.HasMime(version.Codec) {
			response.Header().Set("Content-Type", transcoders.GetMime(version.Codec))
		} else {
			response.Header().Set("Content-Type", transcoders.GetMime(format))
		}
		if format == "raw" {
			response.Header().Set("x-sonicrainboom-transcode", "raw")
		} else {
			response.Header().Set("x-sonicrainboom-transcode", "pretranscoded")
		}
		http.ServeContent(response, request, "", time.Now(), handle)
		_ = handle.Close()

		return
	}
}
