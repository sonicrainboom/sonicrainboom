//+build windows

package api

import "net"

func serve(r http.Handler, silo *silo_server.Silo, initErrors chan error, allSystemsGo chan struct{}) {
	defer timetrack.TimeTrack(time.Now())
	tlsEnabled := true
	conf := config.GetConfig()

	if conf.APIPortTLS == 0 || conf.Cert == "" {
		log.Warnln("TLS disabled, porttls is 0 or no cert was given")
		tlsEnabled = false
	} else if conf.Key == "" {
		conf.Key = conf.Cert
	}

	go func() {
		sig := make(chan os.Signal, 1)
		signal.Notify(sig, syscall.SIGHUP)
		for range sig {
			if silo != nil {
				// Persist current Silo state (when using builtin Silo) before trying to upgrade
				err := silo.Persist()
				if err != nil {
					log.Errorf("State persistence failed:", err)
					continue
				}
			}
			// Try the actual upgrade
			err = upg.Upgrade()
			if err != nil {
				log.Errorf("Upgrade failed:", err)
				continue
			}

			log.Infof("Upgrade succeeded")
		}
	}()

	// Setup HTTP handler
	server := http.Server{
		Handler: r,
	}

	if conf.APIPort != 0 {
		go func() {
			ln, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", conf.APIPort))
			initErrors <- err

			initErrors <- server.Serve(ln)
		}()
	}
	if tlsEnabled {
		go func() {
			ln, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", conf.APIPortTLS))
			initErrors <- err

			initErrors <- server.ServeTLS(ln, conf.Cert, conf.Key)
		}()
	}
}
