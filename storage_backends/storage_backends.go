package storage_backends

import (
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/storage_backends/types"
	"regexp"
	"strings"
	"time"
)

var storageBackends = map[string]types.StorageBackend{}
var storageSchemas = map[string]string{}


func AddBackend(name string, impl types.StorageBackend) {
	defer timetrack.TimeTrack(time.Now())
	if _, ok := storageBackends[name]; !ok {
		storageBackends[name] = impl
		return
	}

	log.Errorf("Storage backend '%s' already registered. Ignoring...")
}

/**
Add support to convert arbitrary schemas to either file, http or https (those can not be added/will not be queried)
*/
func AddSchema(name string, schema string) {
	defer timetrack.TimeTrack(time.Now())
	if _, ok := storageSchemas[schema]; !ok {
		storageSchemas[schema] = name
		return
	}

	log.Errorf("Storage schema '%s' already registered. Ignoring...")
}

func GetBackendByName(name string) (types.StorageBackend, error) {
	defer timetrack.TimeTrack(time.Now())
	if backend, ok := storageBackends[name]; ok {
		return backend, nil
	}
	return nil, types.ENoBackend
}

/**
Guaranteed to return either:
 - file link
 - http(s) link
 - error
*/
func Retrieve(accessor string) (string, types.PermissionSet, error) {
	defer timetrack.TimeTrack(time.Now())
	regex := regexp.MustCompile(`(i)^(file|https?)://`)
	if regex.MatchString(accessor) {
		return accessor, types.PermAll, nil
	}
	splits := strings.Split(accessor, ":")

	if backendName, ok := storageSchemas[splits[0]]; ok {
		backend, err := GetBackendByName(backendName)
		if err != nil {
			return "", types.PermissionSet{}, err
		}
		return backend.Retrieve(accessor)
	}
	return "", types.PermissionSet{}, types.EUnknownScheme
}
