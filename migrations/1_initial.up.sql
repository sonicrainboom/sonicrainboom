START TRANSACTION;

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
    RETURNS TRIGGER AS
$$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

/*users*/
CREATE TABLE "users"
(
    id         serial              NOT NULL PRIMARY KEY,
    name       varchar(32) UNIQUE  NOT NULL,
    email      varchar(254) UNIQUE NOT NULL,
    role       varchar(32)         NOT NULL,
    created_at TIMESTAMPTZ         NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ         NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
    BEFORE UPDATE
    ON "users"
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

INSERT INTO users (id, name, email, role)
VALUES (1, 'nobody', 'nobody@example.com', 'nobody');
INSERT INTO users (id, name, email, role)
VALUES (2, 't4cc0re', 'srb@t4cc0.re', 'superuser');
INSERT INTO users (id, name, email, role)
VALUES (3, 'testuser', 'test@example.com', 'user');

/*credentials*/
CREATE TABLE "credentials"
(
    id       serial       NOT NULL PRIMARY KEY,
    userId   INT          NOT NULL,
    provider varchar(32)  NOT NULL,
    data     varchar(255) NOT NULL,
    FOREIGN KEY (userId) REFERENCES users (id)
        ON DELETE CASCADE,
    UNIQUE (userId, provider)
);


INSERT INTO credentials (id, userId, provider, data)
VALUES (0, 2, 'gitlab.com', '555989');

/* artists */
CREATE TABLE "artists"
(
    id    serial        NOT NULL PRIMARY KEY,
    title varchar(1024) NOT NULL UNIQUE
);

/* albums */
CREATE TABLE "albums"
(
    id       serial        NOT NULL PRIMARY KEY,
    title    varchar(1024) NOT NULL,
    artistId INT           NOT NULL,
    FOREIGN KEY (artistId) REFERENCES artists (id)
        ON DELETE CASCADE,
    UNIQUE (title, artistId)
);

/* playlists */
CREATE TABLE "playlists"
(
    id     serial        NOT NULL PRIMARY KEY,
    title  varchar(1024) NOT NULL,
    userId INT           NOT NULL,
    FOREIGN KEY (userId) REFERENCES users (id)
        ON DELETE CASCADE
);

/* songs */
CREATE TABLE "songs"
(
    id        serial        NOT NULL PRIMARY KEY,
    title     varchar(1024) NOT NULL,
    artistId  INT           NOT NULL,
    albumId   INT           NOT NULL,
    trackNr   INT           NOT NULL DEFAULT 0,
    discNr    INT           NOT NULL DEFAULT 0,
    duration  INT           NOT NULL, /* milliseconds*/
    playCount INT           NOT NULL DEFAULT 0,
    FOREIGN KEY (artistId) REFERENCES artists (id)
        ON DELETE CASCADE,
    FOREIGN KEY (albumId) REFERENCES albums (id)
        ON DELETE CASCADE,
    UNIQUE (title, artistId, albumId, trackNr, discNr, duration)
);

/* versions */
CREATE TABLE "versions"
(
    id            serial        NOT NULL PRIMARY KEY,
    songId        INT           NOT NULL,
    formatId      varchar(64)   NOT NULL, /* used for source, mp3-320, alac, aac-256, etc.*/
    accessor      varchar(1024) NOT NULL UNIQUE, /* contains the accessor such as file://foobar or googledrive://bar/baz */
    codec         varchar(32),
    codecLong     varchar(255),
    channels      int,
    channelLayout varchar(64),
    bitRate       int,
    bitDepth      int,
    sampleRate    int,
    FOREIGN KEY (songId) REFERENCES songs (id)
        ON DELETE CASCADE
);

/* libraries - used to map ids to folders */
CREATE TABLE "libraries"
(
    id   serial        NOT NULL PRIMARY KEY,
    path varchar(1024) NOT NULL UNIQUE
);

/* playlists */
CREATE TABLE "songs_in_playlist"
(
    songId     INT NOT NULL,
    playlistId INT NOT NULL,
    position   INT NOT NULL,
    FOREIGN KEY (playlistId) REFERENCES playlists (id)
        ON DELETE CASCADE,
    FOREIGN KEY (songId) REFERENCES songs (id)
        ON DELETE CASCADE,
    PRIMARY KEY (songId, playlistId)
);


/* probe Queue for scans */
CREATE TABLE "probe_queue"
(
    id       serial        NOT NULL PRIMARY KEY,
    status   INT           NOT NULL DEFAULT 2,
    accessor varchar(1024) NOT NULL UNIQUE /* contains the accessor such as file://foobar or googledrive://bar/baz */
);
CREATE INDEX "probe_queue_status" ON "probe_queue" (status);

/* import queue (bandcamp etc.) */
CREATE TABLE "import_queue"
(
    id     serial        NOT NULL PRIMARY KEY,
    status INT           NOT NULL DEFAULT 2,
    url    varchar(4096) NOT NULL UNIQUE
);
CREATE INDEX "import_queue_status" ON "import_queue" (status);

COMMIT;