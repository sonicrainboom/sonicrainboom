package auth

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/config"
	"net/http"
	"time"
)

const (
	AccessOK         = 1
	AccessDenied     = 0
	AccessAuthPlease = 2
)

func (a *Auth) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("x-sonicrainboom", "true")
		w.Header().Set("x-sonicrainboom-request", fmt.Sprintf("%x", config.GenerateRandomBytes(8)))

		routeName := mux.CurrentRoute(r).GetName()

		var role string
		var restrictedRouteNames []string
		user := a.UserByRequest(r)
		if user != nil {
			//TODO Get from DB
			//TODO Check Issued >= Updated field in DB
			if user.ID == 2 {
				role = "superuser"
			}
			restrictedRouteNames = user.RestrictedRouteNames
		}

		switch a.CheckAccess(routeName, role, restrictedRouteNames) {
		case AccessOK:
			w.Header().Set("x-sonicrainboom-access", "granted")
			next.ServeHTTP(w, r)
		case AccessDenied:
			w.Header().Set("x-sonicrainboom-access", "denied")
			w.WriteHeader(403)
			//TODO: Write nice output depending on headers (json or html)
		case AccessAuthPlease:
			w.Header().Set("x-sonicrainboom-access", "auth-requested")
			w.WriteHeader(401)
			//TODO: Write nice output depending on headers (json or html)
		}
	})
}

func (a *Auth) CheckAccess(routeName string, role string, restrictedRouteNames []string) uint {
	defer timetrack.TimeTrack(time.Now())
	var ok bool
	var acl []string

	a.RWMutex.RLock()
	defer a.RWMutex.RUnlock()

	if acl, ok = a.routeACL[routeName]; !ok || contains(acl, "public") {
		// No ACL, let em pass
		log.Debugf("No/public ACL on %s", routeName)
		return AccessOK
	}

	if len(restrictedRouteNames) > 0 {
		if contains(restrictedRouteNames, routeName) {
			log.Debugf("Restricted route matched for '%s'. Allowed", routeName)
			return AccessOK
		}

		log.Debugf("ACL on %s: %+v, restrictedRouteNames: %s", routeName, acl, restrictedRouteNames)

		return AccessDenied
	}

	if role == "" {
		return AccessAuthPlease
	}

	if contains(acl, role) {
		log.Debugf("ACL matched for '%s'. Allowed", routeName)
		return AccessOK
	}

	log.Debugf("ACL on %s: %+v, Role: %s", routeName, acl, role)

	return AccessDenied
}
