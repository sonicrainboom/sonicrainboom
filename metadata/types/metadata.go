package types

import (
	"errors"
)

type Stream struct {
	Index            uint    `json:"index"`
	CodecName        string  `json:"codec_name"`
	CodecLongName    string  `json:"codec_long_name"`
	CodecType        string  `json:"codec_type"`
	SampleRate       uint64  `json:"sample_rate,string"`
	Channels         uint8   `json:"channels"`
	ChannelLayout    string  `json:"channel_layout"`
	Duration         float32 `json:"duration,string"`
	BitRate          uint64  `json:"bit_rate,string"`
	BitsPerSample    uint64  `json:"bits_per_sample"`
	BitsPerRawSample uint64  `json:"bits_per_raw_sample,string"`
}

type Tags struct {
	Title       string `json:"title"`
	Artist      string `json:"artist"`
	Album       string `json:"album"`
	AlbumArtist string `json:"album_artist"`
	Performer   string `json:"performer"`
	Year        uint16 `json:"year"`
	Date        string `json:"date"`
	Comment     string `json:"comment"`
	Copyright   string `json:"copyright"`
	Encoder     string `json:"encoder"`
	Track       uint   `json:"track,string"`
	Tracktotal  uint   `json:"tracktotal,string"`
	Disc        uint   `json:"disc,string"`
	Disctotal   uint   `json:"disctotal,string"`
}

type Format struct {
	Filename       string  `json:"filename"`
	FormatName     string  `json:"format_name"`
	FormatLongName string  `json:"format_long_name"`
	Duration       float32 `json:"duration,string"`
	Size           uint64  `json:"size,string"`
	BitRate        uint64  `json:"bit_rate,string"`
	Tags           Tags    `json:"tags"`
}

type Metadata struct {
	Streams []Stream `json:"streams"`
	Format  Format   `json:"format"`
}

var ENoExtractor = errors.New("no extractor available")
var ENotSupported = errors.New("file type not supported")

type ExtractorFunc func(path string, meta *Metadata)

func (this *Metadata) HasTags() bool {
	return false
}
