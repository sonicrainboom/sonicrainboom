package metadata

import (
	timetrack "gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata/types"
	"time"
)

var extractors []types.ExtractorFunc

func AddMetadataExtractor(extractor types.ExtractorFunc) {
	defer timetrack.TimeTrack(time.Now())
	extractors = append(extractors, extractor)
}

func ExtractMedatata(path string) (*types.Metadata, error) {
	defer timetrack.TimeTrack(time.Now())
	meta := &types.Metadata{Format: types.Format{Tags: types.Tags{Comment: "TEST"}}}
	for _, extractor := range extractors {
		extractor(path, meta)
	}
	if !meta.HasTags() {
		if meta.Format.Tags.Artist == "" {
			meta.Format.Tags.Artist = "Unknown Artist"
		}
		if meta.Format.Tags.AlbumArtist == "" {
			meta.Format.Tags.AlbumArtist = meta.Format.Tags.Artist
		}
		if meta.Format.Tags.Title == "" {
			meta.Format.Tags.Title = "Unknown Title"
		}
		if meta.Format.Tags.Album == "" {
			meta.Format.Tags.Album = "Unknown Album"
		}
	}
	if meta.Format.Tags.Disc == 0 {
		meta.Format.Tags.Disc = 1
	}

	if meta.Format.Duration == 0 {
		if len(extractors) == 0 {
			return nil, types.ENoExtractor
		} else {
			return nil, types.ENotSupported
		}
	}

	return meta, nil

}
