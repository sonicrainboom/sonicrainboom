package messagehub

import (
	"errors"
	"github.com/gorilla/websocket"
	"gitlab.com/sonicrainboom/sonicrainboom/auth"
	"gitlab.com/sonicrainboom/sonicrainboom/config"
	"net/http"
	"sync"
	"time"
)

type MessageHub struct {
	conns     map[string]*Connection
	endpoints map[string][]func(message Message)
	auth      *auth.Auth
	sync.RWMutex
}

type Message struct {
	Type                  string      `json:"type"`
	Payload               interface{} `json:"payload"`
	RepeatBroadcastToSelf bool        `json:"self,omitempty"`
}

type Connection struct {
	id        string
	invalid   bool // turns true once broken
	conn      *websocket.Conn
	isSystem  bool
	tokenUser *auth.TokenUser
	sync.Mutex
}

var SysConn = &Connection{
	isSystem: true,
	//authUser:      "system",
	//authTokenRole: "admin",
	tokenUser: &auth.TokenUser{
		Username: "system",
	},
}

func (c *Connection) Message(msg Message) (err error) {
	c.Mutex.Lock()
	defer c.Mutex.Unlock()
	if c.invalid || c.isSystem {
		return nil
	}
	if msg.Type == "ping" {
		err = c.conn.WriteMessage(websocket.PingMessage, []byte{})
	} else {
		err = c.conn.WriteJSON(msg)
	}
	if err == websocket.ErrCloseSent {
		c.invalid = true
	}
	return
}

func (c *Connection) Close() (err error) {
	c.Mutex.Lock()
	defer c.Mutex.Unlock()
	err = c.conn.Close()
	c.invalid = true
	return
}

var upgrader = websocket.Upgrader{} // use default options

func (mh *MessageHub) Endpoint(w http.ResponseWriter, r *http.Request) {
	user := mh.auth.UserByRequest(r)
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error(err)
		return
	}
	defer c.Close()
	conn := &Connection{conn: c, id: string(config.GenerateRandomBytes(4))}
	if user != nil {
		conn.tokenUser = user
		log.Infof("Preauthenticated WebSocket via playerauth!")
	}
	mh.Register(conn)
	for {
		var msg Message
		err := c.ReadJSON(&msg)
		if err != nil {
			sendError(conn, err)
			if conn.invalid {
				return
			}
			continue
		}
		err = mh.Message(conn, msg)
		if err != nil {
			sendError(conn, err)
			if conn.invalid {
				return
			}
		}
	}
}

func sendError(connection *Connection, err error) {
	connection.Mutex.Lock()
	defer connection.Mutex.Unlock()
	if websocket.IsUnexpectedCloseError(err) {
		connection.invalid = true
		return
	}
	log.Error(err)
	if err != nil && connection != nil && connection.conn != nil {
		_ = connection.conn.WriteJSON(Message{
			Type:    "error",
			Payload: err.Error(),
		})
	}
}

var Instance *MessageHub

func init() {
	Instance = &MessageHub{conns: map[string]*Connection{}, endpoints: map[string][]func(message Message){}}

	//config.GetConfig()

	go func() {
		for {
			time.Sleep(30 * time.Second)
			_ = Instance.Message(SysConn, Message{Type: "broadcast", Payload: Message{Type: "text", Payload: "Ping from MessageHub"}})
		}
	}()
	go func() {
		for {
			time.Sleep(time.Second)
			_ = Instance.Message(SysConn, Message{Type: "ping"})
		}
	}()
	go Instance.reapInvalid()

}

func Broadcast(message Message) {
	log.WithField("system", "messagehub").Infof("Sending broadcast %+v", message)
	_ = Instance.Message(SysConn, Message{Type: "broadcast", Payload: message})
}

func SetAuth(auth *auth.Auth) {
	(*Instance).auth = auth
	auth.AddACL("ws_broadcast", "admin")
}

func (mh *MessageHub) reapInvalid() {
	var reaped int
	for {
		reaped = 0
		time.Sleep(10 * time.Second)
		mh.RWMutex.Lock()
		for id, conn := range mh.conns {
			conn.Mutex.Lock()
			if conn.invalid {
				delete(mh.conns, id)
				reaped++
			}
			conn.Mutex.Unlock()
		}
		mh.RWMutex.Unlock()
		log.Debugf("Reaped %d websockets", reaped)
	}
}

func (mh *MessageHub) Register(conn *Connection) {
	mh.RWMutex.Lock()
	defer mh.RWMutex.Unlock()
	mh.conns[conn.id] = conn
}

func (mh *MessageHub) RegisterFunc(callback func(message Message), types ...string) {
	mh.RWMutex.Lock()
	defer mh.RWMutex.Unlock()
	for _, msgType := range types {
		mh.endpoints[msgType] = append(mh.endpoints[msgType], callback)
	}
}

func MessageFromPayload(in interface{}) (Message, error) {
	msg, ok := in.(Message)
	if ok {
		return msg, nil
	}
	payload, ok := in.(map[string]interface{})
	if !ok {
		return Message{}, errors.New("not a payload")
	}
	mtype, ok := payload["type"].(string)
	if !ok {
		return Message{}, errors.New("not a payload")
	}
	mpayload, ok := payload["payload"]
	if !ok {
		return Message{}, errors.New("not a payload")
	}
	return Message{Type: mtype, Payload: mpayload}, nil
}

func RolesByPixiedust(user *auth.TokenUser) string {
	if user.ID == 2 {
		return "superuser"
	}
	if user.Username == "system" && user.ID == 0 {
		return "admin"
	}
	return "public"
}

func (mh *MessageHub) Message(source *Connection, msg Message) (err error) {
	defer func() {
		if err := recover(); err != nil {
			log.WithField("error", err).Error("MessageHub.Message panicked!")
		}
	}()

	if msg.Type == "ping" {
		if !source.isSystem {
			log.Warn("ping command from non-system. Ignored")
			return nil
		}

		mh.RWMutex.Lock()
		for _, c := range mh.conns {
			err = c.Message(msg)
			if err != nil {
				log.Warn(err)
			}
		}
		mh.RWMutex.Unlock()
	}

	mh.RWMutex.RLock()
	for _, fun := range mh.endpoints[msg.Type] {
		go fun(msg)
	}
	mh.RWMutex.RUnlock()

	//TODO: Make websockets subscribe to types. Simplifies broadcast handling etc.

	// Handle non-function/special message handlers
	if msg.Type == "authentication" {
		if source.tokenUser == nil {
			_ = source.Message(Message{Type: "authentication_reply", Payload: false})
		} else {
			_ = source.Message(Message{Type: "authentication_reply", Payload: true})
		}
	}

	// "broadcast" broadcasts a message to all clients and the hub
	if msg.Type == "broadcast" {
		if source.tokenUser == nil {
			return errors.New("unauthenticated")
		}
		//TODO: use restricted ACLs
		if mh.auth == nil || mh.auth.CheckAccess("ws_broadcast", RolesByPixiedust(source.tokenUser), nil) != auth.AccessOK {
			return errors.New("unauthorized")
		}
		var bcMsg Message
		bcMsg, err = MessageFromPayload(msg.Payload)
		if err != nil {
			log.Warnf("Broadcast with invalid payload received. %+v", bcMsg)
			return
		}
		mh.RWMutex.Lock()
		for _, c := range mh.conns {
			log.WithField("hub_action", "broadcast").Debugf("%+v", bcMsg)
			err = c.Message(bcMsg)
			if err != nil {
				log.Warn(err)
			}
		}
		// Also send the unwrapped broadcast to local function endpoints
		for _, fun := range mh.endpoints[bcMsg.Type] {
			go fun(bcMsg)
		}
		mh.RWMutex.Unlock()
		return mh.Message(source, bcMsg)
	}

	// "broadcast_user" broadcasts a message to all clients of that user
	if msg.Type == "broadcast_user" {
		if source.tokenUser == nil {
			return errors.New("unauthenticated")
		}
		var bcMsg Message
		bcMsg, err = MessageFromPayload(msg.Payload)
		if err != nil {
			return
		}
		mh.RWMutex.Lock()
		// TODO: Have websockets mapped to user in map
		for _, c := range mh.conns {
			if c.id == source.id && !msg.RepeatBroadcastToSelf {
				continue
			}
			if c.tokenUser.ID == source.tokenUser.ID {
				log.WithField("hub_action", "broadcast_user").WithField("auth_user", source.tokenUser.Username).Debugf("%+v", bcMsg)
				err = c.Message(bcMsg)
				if err != nil {
					log.Warn(err)
				}
			}
		}
		mh.RWMutex.Unlock()
	}

	if msg.Type == "text" {
		log.Info(msg.Payload)
	}
	return nil
}
