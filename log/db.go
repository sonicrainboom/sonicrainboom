package log

import (
	"context"
	pgx "github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
)

type DBLogger struct {
	FieldLogger *logrus.FieldLogger
}

// Go-Migrate
func (dbl DBLogger) Printf(fmt string, v ...interface{}) {
	(*dbl.FieldLogger).Infof(fmt, v...)
}

// Go-Migrate
func (dbl DBLogger) Verbose() bool {
	return true
}

// PQx
func (dbl DBLogger) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	logrusLevel := logrus.ErrorLevel
	switch level {
	default:
		fallthrough
	case pgx.LogLevelNone, pgx.LogLevelError:
		logrusLevel = logrus.ErrorLevel
	case pgx.LogLevelWarn:
		logrusLevel = logrus.WarnLevel
	case pgx.LogLevelInfo:
		logrusLevel = logrus.InfoLevel
	case pgx.LogLevelDebug:
		logrusLevel = logrus.DebugLevel
	case pgx.LogLevelTrace:
		logrusLevel = logrus.TraceLevel
	}

	(*dbl.FieldLogger).WithFields(data).WithContext(ctx).Log(logrusLevel, msg)
}
