package library_management

import (
	"database/sql"
	"fmt"
	"gitlab.com/T4cC0re/time-track"
	"strconv"
	"strings"
	"time"
)

type Album struct {
	Id            int
	Title         string
	AlbumArtistId int
}

type AlbumPopulated struct {
	*Album
	AlbumArtist *Artist
	Format      string
}

func init() {
}

func (t *LibraryManagement) FetchAlbumById(albumId int) (album *Album, err error) {
	defer timetrack.TimeTrack(time.Now())

	var a Album

	err = t.SQLx.Get(&a, t.SQLx.Rebind("SELECT id, title, artistId as albumArtistId FROM albums WHERE id = ?"), albumId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("album", "fetch", "query").Inc()
		log.Errorln(err)
		return
	}
	if a.Id == 0 {
		return nil, ENotFound
	}
	album = &a
	return
}

func (t *LibraryManagement) FetchAlbumPaginated(lastId int) (albums []*Album, maxId int, err error) {
	defer timetrack.TimeTrack(time.Now())
	albums = make([]*Album, 0)

	err = t.SQLx.Select(&albums, t.SQLx.Rebind("SELECT id, title, artistId as albumArtistId FROM albums WHERE id > ? ORDER BY id ASC LIMIT ?"), lastId, PAGINATION_LIMIT)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, 0, ENotFound
		}
		Library.dbErrors.WithLabelValues("album", "page", "query").Inc()
		log.Errorln(err)
		return
	}

	if len(albums) > 0 {
		maxId = albums[len(albums)-1].Id
	} else {
		maxId = lastId
	}
	return
}

func (t *LibraryManagement) FetchAlbumsByArtist(artistId int) (albums []*Album, err error) {
	defer timetrack.TimeTrack(time.Now())
	albums = make([]*Album, 0)

	err = t.SQLx.Select(&albums, t.SQLx.Rebind("SELECT id, title, artistId as albumArtistId FROM albums WHERE artistId = ?"), artistId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("album", "page", "query").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (t *LibraryManagement) FetchAlbumByTitle(title string, artistId int) (album *Album, err error) {
	defer timetrack.TimeTrack(time.Now())
	var a Album

	err = t.SQLx.Get(&a, t.SQLx.Rebind("SELECT id, title, artistId as albumArtistId FROM albums WHERE title = ? AND artistId = ?"), title, artistId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("album", "by_title", "query").Inc()
		log.Errorln(err)
		return
	}
	if a.Id == 0 {
		return nil, ENotFound
	}
	album = &a
	return
}

func (t *LibraryManagement) CreateAlbum(title string, artistId int) (album *Album, err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := t.SQLx.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "create", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	_, err = tx.Exec(t.SQLx.Rebind("INSERT INTO albums (title, artistId) VALUES (?, ?) ON CONFLICT (title, artistId) DO NOTHING"), title, artistId)
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "create", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "create", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return Library.FetchAlbumByTitle(title, artistId)
}

func (t *LibraryManagement) FetchOrCreateAlbum(title string, artist string) (album *Album, err error) {
	defer timetrack.TimeTrack(time.Now())
	var albumArtist *Artist
	albumArtist, err = Library.FetchOrCreateArtist(artist)
	if err != nil {
		log.Infof("FetchOrCreateArtist: '%s'", err.Error())
		return
	}
	album, err = Library.FetchAlbumByTitle(title, albumArtist.Id)
	if album == nil || album.Id == 0 || err != nil {
		log.Infof("AlbumId '%s' does not exist. Creating...", title)
	} else {
		return
	}

	album, err = Library.CreateAlbum(title, albumArtist.Id)
	return
}

//TODO Make SQLX
func (t *LibraryManagement) MergeAlbums(targetId int, subQuery string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "merge", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	albumIdsToMerge := []string{}

	rows, err := tx.Query("SELECT id FROM albums WHERE " + subQuery)
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "merge", "query").Inc()
		log.Errorln(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		err = rows.Scan(&id)
		if err != nil {
			Library.dbErrors.WithLabelValues("album", "merge", "scan").Inc()
			log.Errorln(err)
			return
		}
		albumIdsToMerge = append(albumIdsToMerge, strconv.Itoa(id))
	}
	if len(albumIdsToMerge) < 1 {
		// Nothing to merge, all good!
		return
	}

	print(strings.Join(albumIdsToMerge, ", "))
	err = rows.Err()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "merge", "rows").Inc()
		log.Errorln(err)
		return
	}

	// Hacky, because the db api cannot handle arrays/slices
	_, err = tx.Exec(fmt.Sprintf("UPDATE songs SET albumId = %d WHERE albumId IN ( %s )", targetId, strings.Join(albumIdsToMerge, ", ")))
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "merge", "exec_merge").Inc()
		log.Errorln(err)
		return
	}

	_, err = tx.Exec(fmt.Sprintf("DELETE FROM albums WHERE id IN ( %s )", strings.Join(albumIdsToMerge, ", ")))
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "merge", "exec_delete").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "merge", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

//TODO Make SQLX
func (this *Album) Rename(title string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "rename", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("UPDATE albums SET title = ? WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "rename", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(title, this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "rename", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "rename", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
	return
}

//TODO Make SQLX
func (this *Album) RemoveSong(id int) (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "remove", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("DELETE FROM songs WHERE id = ? AND albumId = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "remove", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(id, this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "remove", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "remove", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

//TODO Make SQLX
func (this *Album) Delete() (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "delete", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("DELETE FROM albums WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "delete", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "delete", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "delete", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (this *Album) Populate() (album *AlbumPopulated, err error) {
	//TODO: caching.

	artist, err := Library.FetchArtistById(this.AlbumArtistId)
	if err != nil {
		return
	}

	versions, err := Library.FindSongVersionByAlbumId(this.Id)
	Library.RankVersionsByQuality(versions)

	album = &AlbumPopulated{Album: this, AlbumArtist: artist, Format: GetFomatByVersions(versions)}
	return
}

func GetFomatByVersions(versions []*SongVersion) (format string) {
	var topBitDepth uint8
	var topSampleRate uint
	var flopBitDepth uint8
	var flopSampleRate uint

	if len(versions) > 0 {
		topBitDepth = versions[0].BitDepth
		topSampleRate = versions[0].SampleRate
		flopBitDepth = versions[len(versions)-1].BitDepth
		flopSampleRate = versions[len(versions)-1].SampleRate
	}

	// DSD is 1 bit with high sample rate
	if topBitDepth == 1 {
		format = fmt.Sprintf("DSD %d MHz", topSampleRate/1000000)
	} else if topBitDepth != 0 {
		format = fmt.Sprintf("PCM %d/%.1f", topBitDepth, float32(topSampleRate)/1000)
		// Cut .0 from format
		if strings.HasSuffix(format, ".0") {
			format = format[:(len(format) - 2)]
		}
	}

	if topSampleRate != flopSampleRate || topBitDepth != flopBitDepth {
		format = "up to " + format
	}

	return
}

func (this *Album) ChangeAlbumArtist(newArtistId int) (err error) {
	if this.AlbumArtistId == newArtistId {
		return
	}

	_, err = Library.FetchArtistById(newArtistId)
	if err != nil {
		return err
	}

	//otherAlbums, err := FetchAlbumsByArtist(newArtistId)
	//if err != nil {
	//	return
	//}
	//albums, err := FetchAlbumsByArtist(this.AlbumArtistId)
	//if err != nil {
	//	return
	//}
	//
	//albums = append(albums, otherAlbums...)

	var other *Album
	var albums []*Album
	other, err = Library.FetchAlbumByTitle(this.Title, newArtistId)
	switch err {
	case nil:
		albums = []*Album{this, other}
	case ENotFound:
		albums = []*Album{this}
	default:
		return
	}

	// Cleanup dupes
	for _, album := range albums {
		var songs []*Song

		// Duplicates (incl. this)
		//if strings.EqualFold(album.Title, this.Title) {

		// Update songs before touching the album
		songs, err = Library.FetchSongsByAlbum(album.Id)
		for _, song := range songs {
			var pop *SongPopulated
			pop, err = song.Populate()
			if pop.Album.AlbumArtistId == this.AlbumArtistId {
				err = pop.editMergingSongsSQL(newArtistId, this.Id, "")
			} else {
				err = pop.editMergingSongsSQL(0, this.Id, "")
			}
			if err != nil {
				return
			}
		}

		// Delete the duplicate
		if album.Id != this.Id {
			err = album.Delete()
			if err != nil {
				return
			}
		}
		//}
	}

	// Merge
	for _, album := range albums {
		//if strings.EqualFold(album.Title, this.Title) {
		if album.AlbumArtistId != newArtistId {
			err = album.changeAlbumArtistSQL(newArtistId)
			if err != nil {
				return
			}
		}
		//}
	}

	return
}

/**
changeAlbumArtistSQL: DO NOT USE DIRECTLY! Call ChangeAlbumArtist instead!
*/
//TODO Make SQLX
func (this *Album) changeAlbumArtistSQL(newArtistId int) (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "artist_change", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("UPDATE albums SET artistId = ? WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "artist_change", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(newArtistId, this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "artist_change", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("album", "artist_change", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}
