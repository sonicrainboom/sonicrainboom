package library_management

import (
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.com/T4cC0re/time-track"
	"gitlab.com/sonicrainboom/sonicrainboom/metadata/types"
	"math"
	"sort"
	"time"
)

type SongVersion struct {
	Id            int
	SongId        int
	Format        string
	Accessor      string
	Codec         string
	CodecLong     string
	Channels      uint8
	ChannelLayout string
	BitRate       uint64
	BitDepth      uint8
	SampleRate    uint
}

var ENoVersion = errors.New("No such version")
var EInvalid = errors.New("Given parameters are invalid")

func init() {
}

func (t *LibraryManagement) FindSongVersionById(versionId int) (songVersion *SongVersion, err error) {
	defer timetrack.TimeTrack(time.Now())

	var sv SongVersion

	err = t.SQLx.Get(&sv, t.SQLx.Rebind("SELECT id, accessor, formatId, songId, codec, codecLong, channels, channelLayout, bitRate, bitDepth, sampleRate FROM versions WHERE id = ?"), versionId)
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "by_accessor", "query").Inc()
		log.Errorln(err)
		return
	}

	if sv.Id == 0 {
		return nil, ENoVersion
	}

	songVersion = &sv
	songVersion.fixBitrate()
	return
}

func (t *LibraryManagement) FindSongVersionByAccessor(accessor string) (songVersion *SongVersion, err error) {
	defer timetrack.TimeTrack(time.Now())

	var sv SongVersion

	err = t.SQLx.Get(&sv, t.SQLx.Rebind("SELECT id, formatId as format, songId, codec, codecLong, channels, channelLayout, bitRate, bitDepth, sampleRate FROM versions WHERE accessor = ?"), accessor)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENoVersion
		}
		Library.dbErrors.WithLabelValues("songversion", "by_accessor", "query").Inc()
		log.Errorln(err)
		return
	}

	if sv.Id == 0 {
		return nil, ENoVersion
	}

	songVersion = &sv
	songVersion.fixBitrate()
	return
}

func (songVersion *SongVersion) fixBitrate() *SongVersion {
	if Library.isLossless(songVersion.Codec) && songVersion.BitRate == 0 {
		songVersion.BitRate = uint64(uint64(songVersion.BitDepth) * uint64(songVersion.SampleRate) * uint64(songVersion.Channels))
	}
	return songVersion
}

func (t *LibraryManagement) FindSongVersionsForSong(songId int) (songVersions []*SongVersion, err error) {
	return Library.FindSingVersionsForSongAndFormat(songId, "")
}

func (t *LibraryManagement) FindSingVersionsForSongAndFormat(songId int, formatId string) (songVersions []*SongVersion, err error) {
	defer timetrack.TimeTrack(time.Now())
	songVersions = make([]*SongVersion, 0)

	if formatId != "" {
		err = t.SQLx.Select(&songVersions, t.SQLx.Rebind("SELECT id, formatId as format, accessor, codec, codecLong, channels, channelLayout, bitRate, bitDepth, sampleRate FROM versions WHERE songId = ? AND formatId = ?"), songId, formatId)
	} else {
		err = t.SQLx.Select(&songVersions, t.SQLx.Rebind("SELECT id, formatId as format, accessor, codec, codecLong, channels, channelLayout, bitRate, bitDepth, sampleRate FROM versions WHERE songId = ?"), songId)
	}
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "by_song", "query").Inc()
		log.Errorln(err)
		return
	}

	if len(songVersions) == 0 {
		return nil, ENoVersion
	}

	for _, sv := range songVersions {
		sv.fixBitrate()
	}

	return
}

func (t *LibraryManagement) FindSongVersionByAlbumId(albumId int) (songVersions []*SongVersion, err error) {
	defer timetrack.TimeTrack(time.Now())
	songVersions = make([]*SongVersion, 0)

	err = t.SQLx.Select(&songVersions, t.SQLx.Rebind("SELECT versions.id, versions.songId, versions.formatId as format, versions.accessor, versions.codec, versions.codecLong, versions.channels, versions.channelLayout, versions.bitRate, versions.bitDepth, versions.sampleRate FROM songs, versions WHERE songs.id = versions.songId AND songs.albumId = ?"), albumId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ENotFound
		}
		Library.dbErrors.WithLabelValues("songversion", "by_album", "query").Inc()
		log.Errorln(err)
		return
	}

	for _, sv := range songVersions {
		sv.fixBitrate()
	}
	return
}

func (t *LibraryManagement) AddSongVersion(songId int, versionId string, accessor string, metadata *types.Stream) (songVersion *SongVersion, err error) {
	defer timetrack.TimeTrack(time.Now())

	if songId == 0 || versionId == "" || accessor == "" {
		return nil, EInvalid
	}

	var codec string
	var codecLong string
	var channels uint8
	var channelLayout string
	var bitRate uint64
	var bitDepth uint8
	var sampleRate uint

	if metadata != nil {
		log.Infof("Metadata stream: %+v", metadata)
		codec = metadata.CodecName
		codecLong = metadata.CodecLongName
		channels = metadata.Channels
		channelLayout = metadata.ChannelLayout
		bitRate = metadata.BitRate
		bitDepth = uint8(math.Max(float64(metadata.BitsPerRawSample), float64(metadata.BitsPerSample)))
		sampleRate = uint(metadata.SampleRate)
	}

	tx, err := t.SQLx.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "add", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	_, err = tx.Exec(
		t.SQLx.Rebind("INSERT INTO versions (songId, formatId, accessor, codec, codecLong, channels, channelLayout, bitRate, bitDepth, sampleRate) "+
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "+
			"ON CONFLICT (accessor) DO NOTHING"), songId, versionId, accessor, codec, codecLong, channels, channelLayout, bitRate, bitDepth, sampleRate)
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "add", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "add", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return Library.FindSongVersionByAccessor(accessor)
}

//TODO SQLx
func (this *SongVersion) Delete() (err error) {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.db.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "delete", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("DELETE FROM versions WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "delete", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "delete", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("songversion", "delete", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}

func (t *LibraryManagement) RankVersionsByQuality(versions []*SongVersion) {
	sort.Slice(versions, func(a, b int) bool {
		aRank := versions[a].BitRate
		bRank := versions[b].BitRate

		if aRank == bRank {
			//TODO: Rank MP3 vs AAC etc.
		}

		return aRank > bRank
	})
}
