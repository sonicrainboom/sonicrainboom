package library_management

import (
	"database/sql"
	timetrack "gitlab.com/T4cC0re/time-track"
	importers_external "gitlab.com/sonicrainboom/sonicrainboom/importers/external"
	importers_types "gitlab.com/sonicrainboom/sonicrainboom/importers/types"
	"runtime"
	"time"
)

const (
	IMPORT_DONE          = 1
	IMPORT_PENDING       = 2
	IMPORT_IN_PROGRESS   = 4
	IMPORT_ERRORED       = 8
	IMPORT_NOT_SUPPORTED = 16
)

//TODO: Make this SQLx

func (t *LibraryManagement) AddImport(url string) (id int64, err error) {
	r, err := t.SQLx.Exec("INSERT INTO import_queue (url, status) VALUES (?, ?)", url, IMPORT_PENDING)
	if err != nil {
		Library.dbErrors.WithLabelValues("import_queue", "create", "query").Inc()
		return
	}
	id, err = r.LastInsertId()
	return
}

func (t *LibraryManagement) import_queueConsumerRunner(c chan *ImportQueueEntry) {
	var err error
	for {
		entry := <-c

		entry.SetInProgress(true)
		err = importers_external.RunImport(entry.URL)
		if err == importers_types.ENoImporter {
			log.Errorln(err)
			entry.SetUnsupported(true)
			continue
		}
		if err != nil {
			log.Errorln(err)
			entry.SetErrored(true)
			continue
		}

		log.Infof("Imported: '%s'", entry.URL)

		entry.SetDone(true)
	}
}

func (t *LibraryManagement) import_queueConsumer() {
	queryFlags := IMPORT_PENDING

	c := make(chan *ImportQueueEntry)

	consumers := 0
	for consumers < runtime.NumCPU()/2 {
		log.Infof("Starting import queue consumer (%d of %d)...", consumers+1, runtime.NumCPU()/2)

		go Library.import_queueConsumerRunner(c)
		consumers++
	}

	for {
		entries, _, _ := Library.FetchImportQueueByToggledFlags(queryFlags, 0)

		for _, entry := range entries {
			entry.SetInProgress(true)
			c <- entry
		}

		if len(entries) == 0 {
			time.Sleep(5 * time.Second)
		}
	}

}

/**
FetchImportQueueByToggledFlags Returns all import_queue items that match flags. If flags is negative, returns items that do NOT match the flags
*/
func (t *LibraryManagement) FetchImportQueueByToggledFlags(flags int, lastId int) (entries []*ImportQueueEntry, maxId int, err error) {
	defer timetrack.TimeTrack(time.Now())
	//var rows *sql.Rows
	entries = make([]*ImportQueueEntry, 0)

	var queryString string

	if flags >= 0 {
		queryString = "SELECT id, url, status FROM import_queue WHERE id > ? AND  (status & ?) = ? ORDER BY id ASC LIMIT ?"

		//rows, err = Library.db.Query("SELECT id, url, status FROM import_queue WHERE id > ? AND  (status & ?) = ? ORDER BY id ASC LIMIT ?", lastId, flags, flags, PAGINATION_LIMIT)
	} else {
		flags *= -1
		queryString = "SELECT id, url, status FROM import_queue WHERE id > ? AND  (status & ?) = ? ORDER BY id ASC LIMIT ?"
		//rows, err = Library.db.Query("SELECT id, url, status FROM import_queue WHERE id > ? AND  (status & ?) != ? ORDER BY id ASC LIMIT ?", lastId, flags, flags, PAGINATION_LIMIT)
	}

	err = t.SQLx.Select(&entries, queryString, lastId, flags, flags, PAGINATION_LIMIT)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, 0, ENotFound
		}
		Library.dbErrors.WithLabelValues("import_queue", "fetch", "query").Inc()
		log.Errorln(err)
		return
	}
	//defer rows.Close()
	//for rows.Next() {
	//	var id int
	//	var accessor string
	//	var status int
	//	err = rows.Scan(&id, &accessor, &status)
	//	if err != nil {
	//		Library.dbErrors.WithLabelValues("import_queue", "fetch", "scan").Inc()
	//		log.Errorln(err)
	//		return
	//	}
	//	entries = append(entries, &ProbeQueueEntry{Id: id, Accessor: accessor, Status: status})
	//	maxId = id
	//}
	//err = rows.Err()
	//if err != nil {
	//	Library.dbErrors.WithLabelValues("import_queue", "fetch", "rows").Inc()
	//	log.Errorln(err)
	//	return
	//}

	return
}

type ImportQueueEntry struct {
	Id     int
	Status int
	URL    string
}

func (this *ImportQueueEntry) SetDone(done bool) {
	if done {
		this.Status |= IMPORT_DONE
		this.Status &= ^(IMPORT_IN_PROGRESS)
		this.Status &= ^(IMPORT_PENDING)
	} else {
		this.Status &= ^(IMPORT_DONE)
	}
	this.saveFlags()
}

func (this *ImportQueueEntry) SetInProgress(inProgress bool) {
	if inProgress {
		this.Status |= IMPORT_IN_PROGRESS
		this.Status &= ^(IMPORT_DONE)
		this.Status &= ^(IMPORT_PENDING)
	} else {
		this.Status &= ^(IMPORT_IN_PROGRESS)
	}
	this.saveFlags()
}

func (this *ImportQueueEntry) SetErrored(errored bool) {
	if errored {
		this.Status |= IMPORT_ERRORED
		this.Status &= ^(IMPORT_DONE)
		this.Status &= ^(IMPORT_PENDING)
	} else {
		this.Status &= ^(IMPORT_ERRORED)
	}
	this.saveFlags()
}

func (this *ImportQueueEntry) SetUnsupported(unsupported bool) {
	if unsupported {
		this.Status |= IMPORT_NOT_SUPPORTED
		this.Status &= ^(IMPORT_DONE)
		this.Status &= ^(IMPORT_PENDING)
		this.Status &= ^(IMPORT_IN_PROGRESS)
	} else {
		this.Status &= ^(IMPORT_NOT_SUPPORTED)
	}
	this.saveFlags()
}

func (this *ImportQueueEntry) saveFlags() {
	defer timetrack.TimeTrack(time.Now())
	tx, err := Library.SQLx.Begin()
	if err != nil {
		Library.dbErrors.WithLabelValues("import_queue", "saveFlags", "tx_begin").Inc()
		log.Errorln(err)
		return
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("UPDATE import_queue SET status = ? WHERE id = ?")
	if err != nil {
		Library.dbErrors.WithLabelValues("import_queue", "saveFlags", "tx_prepare").Inc()
		log.Errorln(err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(this.Status, this.Id)
	if err != nil {
		Library.dbErrors.WithLabelValues("import_queue", "saveFlags", "exec").Inc()
		log.Errorln(err)
		return
	}
	err = tx.Commit()
	if err != nil {
		Library.dbErrors.WithLabelValues("import_queue", "saveFlags", "tx_commit").Inc()
		log.Errorln(err)
		return
	}
	return
}
