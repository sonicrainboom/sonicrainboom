package types

import "errors"

/**
return values:
  canTranscode - bool- Signals whether the input format is supported
  error - error - not nil if an error occurred
*/
type TranscoderFunc func(uri string) (bool, string, error)
type Transcoder struct {
	Priority uint8 // The bigger the more important :)
	Callback TranscoderFunc
	Name     string
}

var ENoTranscoder = errors.New("no transcoder available")
