// +build linux darwin freebsd

package buildinfo

import (
	"fmt"
	"golang.org/x/sys/unix"
	"runtime"
	"strings"
)

func GetRuntimeOSInfo() string {
	osInfo := unix.Utsname{}
	err := unix.Uname(&osInfo)
	if err != nil {
		return runtime.GOOS + " (" + runtime.GOARCH + ")"
	}

	return fmt.Sprintf(
		"%s %s %s %s %s",
		toString(osInfo.Sysname[:]),
		toString(osInfo.Nodename[:]),
		toString(osInfo.Release[:]),
		toString(osInfo.Version[:]),
		toString(osInfo.Machine[:]),
	)
}

func toString(v []byte) string {
	return strings.TrimRight(string(v), "\x00")
}
