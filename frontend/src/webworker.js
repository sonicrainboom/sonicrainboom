var CACHE_VERSION = 1;
var CURRENT_CACHES = {
    srb: 'srb-cache-v' + CACHE_VERSION,
    audio: 'audio',
};

self.addEventListener('install', (event) => {
    event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', function (event) {
    // Delete all caches that aren't named in CURRENT_CACHES.
    // While there is only one cache in this example, the same logic will handle the case where
    // there are multiple versioned caches.
    var expectedCacheNamesSet = new Set(Object.values(CURRENT_CACHES));
    event.waitUntil(
        self.clients.claim()
            .then(caches.keys)
            .then(function (cacheNames) {
                return Promise.all(
                    cacheNames.map(function (cacheName) {
                        if (!expectedCacheNamesSet.has(cacheName)) {
                            // If this cache name isn't present in the set of "expected" cache names, then delete it.
                            console.log('Deleting out of date cache:', cacheName);
                            return caches.delete(cacheName);
                        }
                    })
                );
            })
    );
});

const handlers = {
    isInstalled: async () => {
        return JSON.stringify({installed: true});
    },
    cacheAudio: async () => {

    }
}

/**
 *
 * @param request Request
 * @param handler string
 * @returns {Promise<Response>}
 */
const handleInternal = async (request, handler) => {
    let body = ""
    let init = {
        headers: {
            "content-type": "application/json",
        },
        status: 404,
        statusText: "Not Found",
    }

    if (handlers[handler]) {
        try {
            body = await handlers[handler]()
            init.status = 200;
            init.statusText = "OK"
        } catch (e) {
            init.status = 500;
            init.statusText = "Internal Server Error"
        }
    }

    return new Response(body, init);
}

const regex = /\/api\/worker\/v1\/(.*)$/gm;

const cachedContent = async (request) => {
    const cache = await caches.open(CURRENT_CACHES.srb)
    let response = await cache.match(request);

    if (response) {
        console.log('Found response in cache:', response);

        return response;
    }

    // Otherwise, if there is no entry in the cache for event.request, response will be
    // undefined, and we need to fetch() the resource.
    console.log(' No response for %s found in cache. About to fetch ' +
        'from network...', request.url);

    // We call .clone() on the request since we might use it in a call to cache.put() later on.
    // Both fetch() and cache.put() "consume" the request, so we need to make a copy.
    // (see https://developer.mozilla.org/en-US/docs/Web/API/Request/clone)
    response = await fetch(request.clone())

    console.log('  Response for %s from network is: %O',
        request.url, response);

    if (response.status < 400 &&
        response.headers.has('content-type') &&
        response.headers.get('x-sonicrainboom-workercache') !== 'ignore'
    ) {
        console.log('  Caching the response to', request.url);
        await cache.put(request, response.clone());
    } else {
        console.log('  Not caching the response to', request.url);
    }

    // Return the original response object, which will be used to fulfill the resource request.
    return response;
}

self.addEventListener('fetch', function (event) {
    console.log('Handling fetch event for', event.request.url);

    const re = regex.exec(event.request.url)
    if (re) {
        console.log('internal request.', re[1])
        event.respondWith(handleInternal(event.request, re[1]));
    } else {
        event.respondWith(cachedContent(event.request));
    }
});