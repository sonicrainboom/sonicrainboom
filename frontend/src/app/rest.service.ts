import {Injectable} from '@angular/core';
import {SonicRainBoom} from './sonicrainboom';
import Artist = SonicRainBoom.Artist;
import Paginated = SonicRainBoom.Paginated;
import AlbumPopulated = SonicRainBoom.AlbumPopulated;
import SongPopulated = SonicRainBoom.SongPopulated;
import Song = SonicRainBoom.Song;
import OauthProvider = SonicRainBoom.OauthProvider;

declare global {
    interface Window {
        APIEndpoint: string;
        BaseURL: string;
        SRBURL: string;
    }
}

export class Client {
    constructor() {
    }

    public get = async (url: string): Promise<any> => {
        const result = await fetch(
            url,
            {
                method: "GET",
            },
        );
        return result.json()
    }

    public delete = async (url: string): Promise<any> => {
        const result = await fetch(
            url,
            {
                method: "DELETE",
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        );
        return result.json()
    }

    public post = async (url: string): Promise<any> => {
        const result = await fetch(
            url,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        );
        return result.json()
    }
}

window.APIEndpoint = "";
window.BaseURL = "";

@Injectable({
    providedIn: 'root'
})
export class RestService {
    private artistsCache: Map<number, string> = new Map();
    private albumCache: Map<number, AlbumPopulated> = new Map();

    public get endpoint(): string {
        let baseURL = "https://dev.sonicrainboom.rocks";
        if (window.location) {
            baseURL = `${window.location.protocol}//${window.location.host}`;
            if (window.location.protocol == 'https:') {
                const srbID = window.location.host.match(/^([^.]+)\.instance\.sonicrainboom\.rocks$/) ? window.location.host.replace('instance.sonicrainboom.rocks', '') : window.location.host;
                window.SRBURL = `srb://${srbID}`
            } else {
                window.SRBURL = baseURL
            }
        }
        window.APIEndpoint = `${baseURL}/api/v1/`;
        window.BaseURL = baseURL;
        return window.APIEndpoint;
    }

    public http: Client

    constructor() {
        this.http = new Client();
    }

    public isServiceWorkerInstalled = async (): Promise<boolean> => {
        try {
            const data = await this.http.get('/api/worker/v1/isInstalled')
            return data['installed'];
        } catch (_) {
            return false;
        }
    }

    public titleSortedInsertMultiple = (array: { Title: string }[], ...values: { Title: string }[]) => {
        for (let value of values) {
            this.titleSortedInsert(array, value)
        }
    };

    public titleSortedInsert = (array: { Title: string }[], value: { Title: string }) => {
        let l = 0,
            r = array.length - 1,
            m;
        while (l <= r) {
            m = (l + r) / 2 | 0;
            if (array[m].Title > value.Title) {
                r = m - 1;
                continue;
            }
            l = m + 1;
            if (array[m].Title === value.Title) {
                return;
            }
        }
        array.splice(l, 0, value);

    };

    public getArtists = async (lastId: number = 0): Promise<Paginated<Artist[]>> => {
        if (!lastId) {
            this.artistsCache.clear() // Just in case something got deleted serverside
        }
        let data = await this.http.get(this.endpoint + 'artists?lastId=' + lastId) as Paginated<Artist[]>;
        for (let artist of data.Data) {
            this.artistsCache.set(artist.Id, artist.Title)
        }

        return data;
    };

    public getArtistAlbums = async (id: number = 0): Promise<AlbumPopulated[]> => {
        return await this.http.get(this.endpoint + 'artists/' + id + '/albums') as AlbumPopulated[];
    };

    public loadProviders = async (): Promise<OauthProvider[]> => {
        try {
            return await this.http.get(this.endpoint + 'auth/providers') as OauthProvider[];
        } catch (e) {
            console.log(e);
            return [];
        }
    };

    public isLoggedIn = async (): Promise<boolean> => {
        try {
            const resp = await this.http.get(this.endpoint + 'auth') as any;
            return !!resp.user;
        } catch (_) {
            return false;
        }
    };

    public getArtist = async (id): Promise<Artist> => {
        if (this.artistsCache.has(id)) {
            let title = this.artistsCache.get(id);
            return {Id: id, Title: title} as Artist;
        }
        return await this.http.get(this.endpoint + 'artists/' + id) as Artist;
    };

    public getAlbumPolulated = async (id): Promise<AlbumPopulated> => {
        if (this.albumCache.has(id)) {
            let album = this.albumCache.get(id);
            return album;
        }
        return await this.http.get(this.endpoint + 'albums/' + id) as AlbumPopulated;
    };

    public getPlayerAuth = async (): Promise<string> => {
        const auth = await this.http.get(this.endpoint + 'auth/playerauth') as any;
        if (auth?.auth_token) {
            return auth.auth_token
        } else {
            return ''
        }
    };
    public getSongsByAlbum = async (id): Promise<SongPopulated[]> => {
        return await this.http.get(this.endpoint + 'albums/' + id + '/songs') as SongPopulated[];
    };

    public songComparator = (a: Song, b: Song): number => {
        if (a.DiscNr < b.DiscNr) {
            return -1;
        }
        if (a.DiscNr > b.DiscNr) {
            return 1;
        }
        if (a.TrackNr < b.TrackNr) {
            return -1;
        }
        if (a.TrackNr > b.TrackNr) {
            return 1;
        }
        return 0;
    };

    // public addProduct = (product): Observable<any> => {
    //     console.log(product);
    //     return this.http.post<any>(endpoint + 'products', JSON.stringify(product), httpOptions).pipe(
    //         tap((product) => console.log(`added product w/ id=${product.id}`)),
    //         catchError(this.handleError<any>('addProduct'))
    //     );
    // };

    // public updateProduct = (id, product): Observable<any> => {
    //     return this.http.put(endpoint + 'products/' + id, JSON.stringify(product), httpOptions).pipe(
    //         tap(_ => console.log(`updated product id=${id}`)),
    //         catchError(this.handleError<any>('updateProduct'))
    //     );
    // };

    public deleteArtist = async (id) => {
        return await this.http.delete(this.endpoint + 'artists/' + id);
    };
}
