import {Component, OnInit} from '@angular/core';
import {RestService} from "@src/app/rest.service";
import {ActivatedRoute} from "@angular/router";
import {SonicRainBoom} from "@src/app/sonicrainboom.d";
import AlbumPopulated = SonicRainBoom.AlbumPopulated;
import SongPopulated = SonicRainBoom.SongPopulated;
import Song = SonicRainBoom.Song;
import {PlayerComponent} from "@src/app/player/player.component";

@Component({
    selector: 'app-album-detail',
    templateUrl: './album-detail.component.html',
    styleUrls: ['./album-detail.component.scss']
})
export class AlbumDetailComponent implements OnInit {
    id: number;
    album: AlbumPopulated = {Id: 0, Title: "", AlbumArtist: {Id: 0, Title: ""}, AlbumArtistId: 0, Format: ""};
    songs: SongPopulated[] = [];

    constructor(private rest: RestService, private player: PlayerComponent, private _route: ActivatedRoute) {
    }

    ngOnInit() {
        this.id = parseInt(this._route.snapshot.paramMap.get('id'), 10);
        this.getArtist();
    }

    private getArtist = async () => {
        this.album = await this.rest.getAlbumPolulated(this.id);
        this.songs = await this.rest.getSongsByAlbum(this.id);
        this.songs.sort(this.rest.songComparator)
    };
}
