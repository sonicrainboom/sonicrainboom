import {TNSPlayer} from 'nativescript-audio-player';

export class Specifics {
    private _player: TNSPlayer;
    private _endcallback: any;

    constructor() {
        this._player = new TNSPlayer();
        this._player.debug = true; // set true to enable TNSPlayer console logs for debugging.
    }

    public play = async (url: string) => {
        // let platform = require("tns-core-modules/platform");
        // console.log("Specific NativeScript on " + platform.device.os);
        // alert("Specific NativeScript on " + platform.device.os);
        // await this._player.dispose(); // is this required?
        if (url) {
            await this.preload(url);
        }
        await this._player.play()
    };

    public preload = async (url: string) => {
        await this._player.initFromUrl({
            audioFile: url,
            autoPlay: false,
            completeCallback: this._trackComplete.bind(this),
            errorCallback: this._trackError.bind(this),
        } as any)
    };

    private _trackComplete(args: any) {
        console.log('reference back to player:', args.player);
        // iOS only: flag indicating if completed succesfully
        console.log('whether song play completed successfully:', args.flag);
        if (this._endcallback) {
            this._endcallback();
        }
    }

    private _trackError(args: any) {
        console.log('reference back to player:', args.player);
        console.log('the error:', args.error);
        // Android only: extra detail on error
        console.log('extra info on the error:', args.extra);
        if (this._endcallback) {
            this._endcallback();
        }
    }

    public pause = async () => {
        return this._player.pause()
    }

    public tooglePause = async () => {
        if (this._player.isAudioPlaying()) {
            await this._player.pause()
        } else {
            this._player.resume()
        }
    };

    public isPaused = (): boolean => {
        return !this._player.isAudioPlaying()
    };

    public setVolume = (percent: number) => {
        this._player.volume = percent / 100
    };

    public getVolume = (): number => {
        return Math.min(this._player.volume * 100, 100);
    };

    public getDuration = (): number => {
        return 0
    };

    public getCurrentTime = () => {
        return this._player.currentTime;
    };

    public setCurrentTime = (seconds: number) => {
        this._player.seekTo(seconds)
    };

    public setEndcallback = (callback: any) => {
        this._endcallback = callback;
    }
}

export let specifics = new Specifics();
