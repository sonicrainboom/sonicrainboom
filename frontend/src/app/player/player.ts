import {specifics} from "@src/app/player/specifics/specifics";
import {RestService} from "@src/app/rest.service";
import {SonicRainBoom} from "@src/app/sonicrainboom.d";
import SongPopulated = SonicRainBoom.SongPopulated;

declare global {
    interface Window {
        player: Player;
    }
}

export class Player {

    private song: SongPopulated;
    private queue: Array<SongPopulated>;

    constructor(private rest: RestService) {
        console.log("Player constructed");
        // This is a hack to get Angular to update the seek slider when there is no input
        setInterval(() => this.currentTime, 250);
        specifics.setEndcallback(this.playbackDone);
        this.queue = [];
        this.storageEstimateWrapper().then(a => {
            console.log(`Navigator.Storage:`, a)
        })

        // (window as any).player = this;
    }

    private detectSupportedFormat = async (): Promise<string> => {
        //TODO: actual detection
        return "wav" // Sensible default
    };

    /**
     * This will call `played` on the song. This will trigger any backend actions depending on tracking media playback
     * for 3rd party providers. E.g. a last.fm scrobble, if implemented and enabled.
     * @param song
     */
    public trackPlayback = async (song: SongPopulated) => {
        let url = this.rest.endpoint + `songs/${song.Id}/played`;
        await this.rest.http.post(url);
    };

    public skip = async () => {
        return this.playbackDone();
    };

    private playbackDone = async () => {
        console.log(this.song);
        if (this.song) {
            this.trackPlayback(this.song);
            window.MessageHub.send_broadcast_user("playback_done", this.song.Id);
            try {
                URL.revokeObjectURL(this.song.CachedURL);
            } catch (_) {
            }
        }
        console.log(this.queue);
        const nextSong = this.queue.shift();
        console.log(nextSong);
        if (nextSong) {
            this.playSong(nextSong);
        }
        console.log(this.queue.length);
        if (this.queue.length > 0) {
            this.queue[0] = await this.prepareSong(this.queue[0]);
        }
    }

    /**
     * This will issue a HEAD request to the API. This will do any on-the-fly conversions, so that a `playSong` call
     * would immediately (+request latency) get the song in the desired format.
     * The primary use for this is to prepare the next song in a playlist.
     * @param song
     */
    public prepareSong = async (song: SongPopulated): Promise<SongPopulated> => {
        song.CachedURL = await this.preCache(song);
        return song
    };

    public enqueueSong = async (...songs: SongPopulated[]) => {
        console.log(songs, this.queue);
        this.queue.push(...songs);
        window.MessageHub.send_broadcast_user("queue_update", this.queue);
    };

    public prepareLocalCache = async () => {
        if (!('storage' in navigator)) {
            // @ts-ignore
            await navigator.storage.persisted()
        }
    };

    public copyLink = async (song: SongPopulated) => {
        const el = document.createElement('textarea');
        const auth = await this.rest.getPlayerAuth();
        if (!auth) {
            alert("Unable to authenticate. Are you logged in?");
        }
        el.value = `${window.SRBURL}/song/${song.Id}#${auth}`;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    public copyConnectLink = async () => {
        const el = document.createElement('textarea');
        const auth = await this.rest.getPlayerAuth();
        if (!auth) {
            alert("Unable to authenticate. Are you logged in?");
        }
        el.value = `${window.SRBURL}/connect#${auth}`;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    public storageEstimateWrapper = async () => {
        if ('storage' in navigator && 'estimate' in navigator.storage) {
            // We've got the real thing! Return its response.
            return navigator.storage.estimate();
        }

        if ('webkitTemporaryStorage' in navigator &&
            // @ts-ignore
            'queryUsageAndQuota' in navigator.webkitTemporaryStorage) {
            // Return a promise-based wrapper that will follow the expected interface.
            return new Promise(function (resolve, reject) {
                // @ts-ignore
                navigator.webkitTemporaryStorage.queryUsageAndQuota(
                    function (usage, quota) {
                        resolve({usage: usage, quota: quota})
                    },
                    reject
                );
            });
        }

        // If we can't estimate the values, return a Promise that resolves with NaN.
        return Promise.resolve({usage: NaN, quota: NaN});
    }

    private getURL = async (song: SongPopulated): Promise<string> => {
        return this.rest.endpoint + `songs/${song.Id}/play/${await this.detectSupportedFormat()}`
    };

    public preCache = async (song: SongPopulated): Promise<string> => {
        if (song.CachedURL) {
            return song.CachedURL
        }
        const url = await this.getURL(song)
        const response = await fetch(
            url,
            {
                credentials: 'same-origin',
                headers: {
                    "x-sonicrainboom-preflight": "yes"
                },
                method: 'GET',
                mode: 'same-origin',
                cache: 'force-cache',
                redirect: 'follow',
                referrer: '',
            }
        );

        const blob = await response.blob()
        return URL.createObjectURL(blob)
    }

    public formatSong = () => {
      if (!this.song) {
        return "SonicRainBoom";
      }

      return `${this.currentArtist} - ${this.currentTitle}`;
    }

    public playSong = async (song: SongPopulated) => {
        const url = song.CachedURL || await this.getURL(song)

        await specifics.play(url);
        this.song = song;
        document.title = this.formatSong();
        window.MessageHub.send_broadcast_user("playback", song.Id);

        if ('mediaSession' in navigator) {
            // @ts-ignore
            navigator.mediaSession.metadata = new MediaMetadata({
                title: this.currentTitle,
                artist: this.currentArtist,
                album: this.currentAlbum,
                artwork: [
                    {src: `${this.rest.endpoint}albums/${song.AlbumId}/cover`},
                ]
            });

            // @ts-ignore
            navigator.mediaSession.setActionHandler('play', this.play);
            // @ts-ignore
            navigator.mediaSession.setActionHandler('pause', this.pause);
            // @ts-ignore
            navigator.mediaSession.setActionHandler('nexttrack', this.skip);

            // TODO: Use navigator.mediaSession.playbackState for playback detection
        }
        return
    };

    public tooglePause = async () => {
        return specifics.tooglePause()
    };

    public get paused(): boolean {
        return specifics.isPaused()
    }

    public set volume(percent: number) {
        specifics.setVolume(percent);
    };

    public get volume(): number {
        return specifics.getVolume();
    };

    public get duration(): number {
        return specifics.getDuration();
    };

    public set currentTime(seconds: number) {
        specifics.setCurrentTime(seconds);
    };

    public get currentTime(): number {
        return specifics.getCurrentTime();
    };

    public get currentId(): number {
        if (!this.song) {
            return 0;
        }
        return this.song.Id || 0;
    };

    public get currentTitle(): string {
        if (!this.song) {
            return "";
        }
        return this.song.Title || "";
    };

    public get currentArtist(): string {
        if (!this.song) {
            return "";
        }
        if (!this.song.Artist) {
            return "";
        }
        return this.song.Artist.Title || "";
    };

    public get currentAlbum(): string {
        if (!this.song) {
            return "";
        }
        if (!this.song.Album) {
            return "";
        }
        return this.song.Album.Title || "";
    };
}
