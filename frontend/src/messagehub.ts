declare global {
    interface Window {
        MessageHub: MessageHub;
    }
}

export class MessageHub {
    private ws: WebSocket;
    private authenticated: boolean;
    private opened: boolean;
    private handlers: Map<string, Function[]> = new Map<string, Function[]>();

    constructor() {
        this.connect(`${window.location.protocol === "https:" ? "wss" : "ws"}://${window.location.hostname}/api/v1/messagehub`)
    }

    public print = (message) => {
        console.log(message);
    };

    public authenticate = () => {
        this.ws.send(JSON.stringify({type: "authentication"}));
    };

    public on = (event: string, handler: Function) => {
        if (this.handlers.has(event)) {
            this.handlers[event].push(handler);
        } else {
            this.handlers[event] = [handler];
        }
    }

    private emit = async (event: string, payload: any) => {
        if (!this.handlers.has(event)) {
            console.error("Unknown event", event, payload);
            return
        }
        for (let callback of this.handlers[event]) {
            callback(payload);
        }
    }

    private handler = (event) => {
        const message = JSON.parse(event.data);
        switch (message.type) {
            case    "authentication_reply":
                if (message.payload === true) {
                    this.print("AUTHENTICATED");
                } else {
                    this.print("AUTH FAILED!")
                }
                break;
            case "text":
                this.print(`TEXT: ${message.payload}`);
                break;
            case "close":
                this.print("SERVER SAYS GOODBYE");
                this.ws.close();
                break;
            default:
                this.emit(message.type, message.payload); // ignore promise
                console.log("unknown message", message);
                break;
        }
    };

    public connect = (uri) => {
        this.authenticated = false;
        this.ws = new WebSocket(uri);
        this.ws.onopen = () => {
            this.opened = true;
            this.print("OPEN");
            this.authenticate();
        };
        this.ws.onclose = () => {
            this.print("CLOSE");
            setTimeout(() => {
                this.connect(uri)
            }, 2500);
        };
        this.ws.onmessage = this.handler;
        this.ws.onerror = (evt) => {
            this.print("ERROR: " + (evt as any).data);
        };
    };

    public send = (type: string, payload: any) => {
        let message = JSON.stringify({type: type, payload: payload});
        console.log("SENT", message);
        this.ws.send(message);
    };

    public send_broadcast = (type: string, payload: any) => {
        let message = JSON.stringify({type: "broadcast", self: true, payload: {type: type, payload: payload}});
        console.log("SENT", message);
        this.ws.send(message);
    };

    public send_broadcast_user = (type: string, payload: any) => {
        let message = JSON.stringify({type: "broadcast_user", self: true, payload: {type: type, payload: payload}});
        console.log("SENT", message);
        this.ws.send(message);
    };
}
